package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();

    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

   public String randomchoice() {
       Random random = new Random();
       String computerchoice = rpsChoices.get(random.nextInt(rpsChoices.size()));
       return computerchoice;
   }

   public String userchoice() {
       while (true) {
           String userchoice = readInput("Your choice (Rock/Paper/Scissors)?");
           if (rpsChoices.contains(userchoice)) {
               return userchoice;
           }
            else {
                System.out.println("I don't understand " + userchoice + ". Try again.");
            }
           
       }
   }

   public boolean definewinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
           return choice2.equals("rock");
        }
        else if (choice1.equals("rock")) {
            return choice2.equals("scissors");
        }
        else {
            return choice2.equals("paper");
        }
       }
   
    public String continuation() {
        while (true) {
            String yes_or_no = readInput("Do you wish to continue playing? (y/n)?");
            if (yes_or_no.equals("y") || yes_or_no.equals("n")) {
                return yes_or_no;
            }
            else {
                System.out.println("I don't understand " + yes_or_no + ". Try again." );
            }
        }
    }

    public void run() {
        while (true) {
            System.out.printf("%s %d%n", "Let's play round", roundCounter);
            String computerchoice = randomchoice();
            String humanchoice = userchoice();
            String battle = "Human chose " + humanchoice + ", computer chose " + computerchoice + ".";
            if (definewinner(humanchoice, computerchoice)) {
                System.out.println(battle + "Human wins.");
                humanScore++;
            }
            else if (definewinner(computerchoice, humanchoice)) {
                System.out.println(battle + "Computer wins.");
                computerScore++;
            }
            else {
                System.out.println(battle + " It's a tie");
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            String continueanswer = continuation();
            if (continueanswer.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
        roundCounter++;
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
